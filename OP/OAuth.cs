﻿using System;
using System.Text;

namespace OP
{
	class OAuth {
		public static readonly string Version = "1.0";
		private static readonly string AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		private static Random Rnd = new Random();

		public static string Nonce() {

			int len = 16;
			StringBuilder sb = new StringBuilder(len);

			for(int i = 0; i < len; i++) {
				sb.Append(OAuth.AB[OAuth.Rnd.Next(OAuth.AB.Length)]);
			}

			byte[] random = Tools.arrayConcat(Encoding.ASCII.GetBytes(sb.ToString()), Encoding.ASCII.GetBytes(Timestamp().ToString()));

			return Tools.MD5String (random);
		}

		public static string GetNonce() {
			return new Random().Next(123400, 9999999).ToString();
		}

		public static Int32 Timestamp() {
			return (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
		}
	}
}