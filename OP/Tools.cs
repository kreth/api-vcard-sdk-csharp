﻿using System;
using System.IO;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.Security.Cryptography;


namespace OP
{
	class Tools {
		private static readonly string[] UriRfc3986CharsToEscape = new[] { "!", "*", "'", "(", ")" };

		public static string Escape(string data)
		{
			StringBuilder escaped = new StringBuilder (Uri.EscapeDataString(data));

			for (int i = 0; i < UriRfc3986CharsToEscape.Length; i++) {
				escaped.Replace(UriRfc3986CharsToEscape[i], Uri.HexEscape(UriRfc3986CharsToEscape[i][0]));
			}

			return escaped.ToString ();
		}

		public static string DictionaryToString(IDictionary<string, string> dict, Boolean urlEncode = true) 
		{
			StringBuilder sb = new StringBuilder ();
			SortedDictionary<string, string> sorted = new SortedDictionary<string, string> (dict);
			string key = null;
			string val = null;
			foreach (KeyValuePair<string, string> element in sorted) 
			{
				if (sb.Length > 0) {
					sb.Append ("&");
				}

				key = urlEncode ? Escape (element.Key) : element.Key;
				val = urlEncode ? Escape (element.Value) : element.Value;

				try {
					sb.Append((key != null ? key : ""));
					sb.Append("=");
					sb.Append((val != null ? val : ""));
				} catch (UriFormatException e) {
					throw new SystemException ("This method requires UTF-8 encoding support.", e);
				}
			}

			return sb.ToString();
		}

		public static IDictionary<string, string> StringToDictionary(string input) 
		{
			IDictionary<string, string> dict = new Dictionary<string, string> ();

			dict = input.Split('&')
				.Select(x => x.Split('='))
				.Where(x => x.Length > 1 && !String.IsNullOrEmpty(x[0].Trim())
					&& !String.IsNullOrEmpty(x[1].Trim()))
				.ToDictionary(x => x[0].Trim(), x => x[1].Trim());

			return dict;
		}

		public static string Hash(string data, string secret)
		{
			System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
			byte[] keyByte = encoding.GetBytes (secret);

			HMACSHA1 hmacsha1 = new HMACSHA1 (keyByte);

			byte[] dataByte = encoding.GetBytes (data);
			byte[] hashByte = hmacsha1.ComputeHash (dataByte);
			string hashStr = GetBase64 (hashByte);

			return hashStr;
		}

		public static byte[] Encrypt(string encryptionKey, byte[] data)
		{
			byte[] key = MD5Byte (encryptionKey);

			using (MemoryStream ms = new MemoryStream())
			{
				using (AesManaged cryptor = new AesManaged())
				{
					cryptor.Mode = CipherMode.CBC;
					cryptor.Padding = PaddingMode.PKCS7;
					cryptor.KeySize = 128;
					cryptor.BlockSize = 128;

					// We use the random generated iv created by AesManaged
					byte[] iv = cryptor.IV;

					using (CryptoStream cs = new CryptoStream(ms, cryptor.CreateEncryptor(key, iv), CryptoStreamMode.Write))
					{
						cs.Write(data, 0, data.Length);
					}
					byte[] encryptedContent = ms.ToArray();

					// Create new byte array that should contain both unencrypted iv and encrypted data
					byte[] result = new byte[iv.Length + encryptedContent.Length];

					// Copy our 2 array into one
					System.Buffer.BlockCopy(iv, 0, result, 0, iv.Length);
					System.Buffer.BlockCopy(encryptedContent, 0, result, iv.Length, encryptedContent.Length);

					return result;
				}
			}
		}

		public static void Log (string label, string data) {
			Console.Write ("[" + label + "] : ");
			Console.WriteLine("`" + data + "`");
		}

		public static string GetBase64(byte[] data)
		{
			return Convert.ToBase64String(data).Trim();
		}

		public static string MD5String(byte[] data) {
			StringBuilder dataStr = new StringBuilder ();

			MD5 md5 = MD5.Create ();
			byte[] hash = md5.ComputeHash (data);

			for (int i = 0; i < hash.Length; i ++ ) {
				dataStr.Append(hash[i].ToString("X2"));
			}

			return dataStr.ToString();
		}

		public static byte[] MD5Byte(string input) {
			MD5 md5 = MD5.Create ();
			byte[] data = md5.ComputeHash (Encoding.UTF8.GetBytes(input));

			return data;
		}

		public static byte[] arrayConcat(byte[] a1, byte[] a2)
		{
			int a1Size = a1.Length;
			int a2Size = a2.Length;
			byte[] result = new byte[a1Size + a2Size];
			Array.Copy (a1, 0, result, 0, a1Size);
			Array.Copy (a2, 0, result, a1Size, a2Size);

			return result;
		}
	}
}