﻿using System;

namespace OP
{
	public interface IConnectionSession
	{
		Boolean Set (string key, string value);

		Boolean Set (string key, string value, long expires);

		string Get (string key);
	}
}