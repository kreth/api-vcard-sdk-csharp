﻿using System;
using System.Text;
using System.Collections.Generic;

namespace OP
{
	class RequestToken : Token {
		private string method = "POST";
		private string api = "oauth/request/token";

		public RequestToken(string host, string key, string secret, string user, string password) 
		{
			this.Host = host;
			this.Key = key;
			this.Secret = secret;

			string url = host + "/" + api;

			IDictionary<string, string> payload = new Dictionary<string, string>();
			payload.Add("oauth_consumer_key", key);
			payload.Add("oauth_nonce", OAuth.Nonce());
			payload.Add("oauth_timestamp", OAuth.Timestamp().ToString());
			payload.Add("oauth_signature_method", Signature.Method);
			payload.Add("oauth_user_name", user);
			payload.Add("oauth_user_password", password);
			payload.Add("oauth_version", OAuth.Version);

			string signature = new Signature (secret, method, url, payload).String ();
			payload.Add ("oauth_signature", signature);

			//byte[] encryptionKey = Tools.MD5Byte (signature + secret); 
			string encryptionKey = signature + secret;
			payload["oauth_user_name"] = Tools.GetBase64 (Tools.Encrypt (encryptionKey, Encoding.UTF8.GetBytes(user)));
			payload["oauth_user_password"] = Tools.GetBase64 (Tools.Encrypt (encryptionKey, Encoding.UTF8.GetBytes(password)));

			Response = Connection.Request(method, url, payload);
		}
	}
}