﻿using System;

using Newtonsoft.Json.Linq;

namespace OP
{
	class Token {

		public Token() {}

		public void Initialize(string host, string key, string secret) {
			Host = host;
			Key = key;
			Secret = secret;
		}

		public string Host { get; set; }
		public string Key { get; set; }
		public string Secret { get; set; }
		public JObject Response { get; set; }
	}
}